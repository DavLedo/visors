﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Astral;
using Astral.Device;

namespace TestingCalibration
{
    public enum ParameterChangeType
    {
        GravityAccelerationX,
        GravityAccelerationY,
        GravityAccelerationZ,
        LinearAccelerationX,
        LinearAccelerationY,
        LinearAccelerationZ,
        GyroX,
        GyroY,
        GyroZ,
        OrientationPitch,
        OrientationRoll,
        OrientationYaw,
        Compass,
        MicrophoneAmplitude,
        AmbientLight
    }

    public class SensorRange
    {
        public double Low { get; set; }
        public double High { get; set; }

        public double Range
        {
            get
            {
                return Math.Abs(High - Low);
            }
        }

        public SensorRange()
        {
            this.Low = 100000000;
            this.High = -100000000;
        }

        public SensorRange(double low, double high)
        {
            this.Low = low;
            this.High = high;
        }
    }

    public class CalibrationModel
    {
        private string name;
        private Dictionary<ParameterChangeType, SensorRange> sensorRange = new Dictionary<ParameterChangeType, SensorRange>();

        public void UpdateSensor(ParameterChangeType sensorParameter, double value)
        {
            if (!this.sensorRange.ContainsKey(sensorParameter))
            {
                this.sensorRange.Add(sensorParameter, new SensorRange());
            }
            else
            {
                double low = this.sensorRange[sensorParameter].Low;
                double high = this.sensorRange[sensorParameter].High;
             //   Console.WriteLine("{0:00}, {1:00}, {2:00}", value, this.sensorRange[sensorParameter].Low, this.sensorRange[sensorParameter].High);

                if (value < low)
                {
                    this.sensorRange[sensorParameter].Low = value;
                }
                
                if(value > high)
                {
                    this.sensorRange[sensorParameter].High = value;
                }
                if(sensorParameter == ParameterChangeType.AmbientLight)
                    Console.WriteLine(100 * Math.Abs(value) / this.sensorRange[sensorParameter].Range);
            }

        }

        public CalibrationModel(DeviceModel d)
        {
            this.name = d.Name;
            if(d.Device.HasCompass)
            {
                this.sensorRange.Add(ParameterChangeType.Compass, new SensorRange(0, 360)); 
            }
        }
    }
}
