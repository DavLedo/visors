﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Astral;
using Astral.Device;



using Microsoft.Win32;



using WebEye.Controls.Wpf;


namespace TestingCalibration
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CalibrationWindow : Window
    {
        #region Instance Variables

        private NetworkManager networkManager = NetworkManager.Instance;
        private DeviceModel device;

        private CalibrationModel calib;

        Dictionary<ParameterChangeType, double> sensorValue = new Dictionary<ParameterChangeType, double>();

        public class SensorStamp
        {
            public ParameterChangeType Param { get; set; }
            public double Value { get; set; }
            public int Stamp { get; set; }
        }

        List<SensorStamp> sensorStamp = new List<SensorStamp>();

        #endregion

        #region Constructor

        public CalibrationWindow()
        {
            InitializeComponent();

            // Start the networking
            this.networkManager.Start();
            this.networkManager.DeviceAdded += OnDeviceConnected;

            this.Loaded += CalibrationWindow_Loaded;


            DispatcherTimer t = new DispatcherTimer();
            t.Interval = TimeSpan.FromMilliseconds(500);
            t.Tick += OnTick;
            this.StartButton.Click += (s, e) => { t.Start(); };
            this.StopButton.Click += (s, e) => 
            {
                t.Stop();
                List<SensorStamp> accelX = this.sensorStamp.Where(x => x.Param == ParameterChangeType.LinearAccelerationX).ToList();
                double lowest = accelX.Select(x => x.Value).ToList().Min();
                double highest = accelX.Select(x => x.Value).ToList().Max();
                double range = Math.Abs(highest - lowest);
                double ave = Math.Abs(accelX.Average(p => p.Value));
                Console.WriteLine("{0:00}, {1:00}, {2:00}", range, ave, Math.Abs(100 * ave)/range);
                foreach(SensorStamp stamp in accelX)
                {
                 //   Console.WriteLine(Math.Abs(stamp.Value) * 100 / range);
                }
            };
        }

        private void CalibrationWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        #endregion

        #region Connection Initialization

        private void OnDeviceConnected(object sender, DeviceConnectedEventArgs e)
        {
            device = new DeviceModel(e.Device, e.Session);
            this.calib = new CalibrationModel(device);

            // Note: all networking events are running in a separate thread
            // you have to use this dispatcher invoke to do anything with the UI
            // otherwise you'll get a weird exception you can't make sense of
            Dispatcher.Invoke(new Action(delegate
            {

            }));

            InitializeDeviceEvents();


        }

        int stamp = 0;

        private void OnTick(object sender, EventArgs e)
        {
            foreach(ParameterChangeType p in this.sensorValue.Keys)
            {
                this.sensorStamp.Add(new SensorStamp { Param = p, Stamp = stamp, Value = sensorValue[p] });

            }
            stamp++;
        }

        private void InitializeDeviceEvents()
        {
            device.Accelerometer.AccelerationChanged += AccelerometerUpdated;
            device.Microphone.MicrophoneUpdated += OnMicrophoneUpdated;
            device.Gyroscope.RotationChanged += OnGyroUpdated;
            device.AmbientLight.AmbientLightChanged += OnAmbientLightUpdated;
            device.Orientation.OrientationChanged += OnOrientationUpdated;
        }

        private void OnOrientationUpdated(object sender, AstralOrientationEventArgs e)
        {
            //calib.UpdateSensor(ParameterChangeType.OrientationPitch, e.OrientationData.PitchDegrees);
            //calib.UpdateSensor(ParameterChangeType.OrientationRoll, e.OrientationData.RollDegrees);
            //calib.UpdateSensor(ParameterChangeType.OrientationYaw, e.OrientationData.YawDegrees);
        }

        private void OnAmbientLightUpdated(object sender, AstralAmbientLightEventArgs e)
        {
            //calib.UpdateSensor(ParameterChangeType.AmbientLight, e.AmbientLightData.AmbientLight);
        }

        private void OnGyroUpdated(object sender, AstralGyroscopeEventArgs e)
        {
            //calib.UpdateSensor(ParameterChangeType.GyroX, e.GyroscopeData.X);
            //calib.UpdateSensor(ParameterChangeType.GyroY, e.GyroscopeData.Y);
            //calib.UpdateSensor(ParameterChangeType.GyroZ, e.GyroscopeData.Z);
        }

        #endregion

        private void AccelerometerUpdated(object sender, AstralAccelerometerEventArgs e)
        {
            //calib.UpdateSensor(ParameterChangeType.AccelerationX, e.AccelerationData.X);
            //calib.UpdateSensor(ParameterChangeType.AccelerationY, e.AccelerationData.Y);
            //calib.UpdateSensor(ParameterChangeType.AccelerationZ, e.AccelerationData.Z);
            this.sensorValue[ParameterChangeType.LinearAccelerationX] = e.AccelerationData.Y;

        }

        private void OnMicrophoneUpdated(object sender, AstralMicrophoneEventArgs e)
        {
            //calib.UpdateSensor(ParameterChangeType.MicrophoneAmplitude, e.MicrophoneData.Amplitude);
        }

    }
}
