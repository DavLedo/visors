﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;

namespace Visors
{
    public class Constants
    {
        public static SolidColorBrush Turquoise = Utils.GetSolidColorBrush("#FF2EB1BF");
        public static SolidColorBrush Blue = Utils.GetSolidColorBrush("#FF334D5C");
        public static SolidColorBrush Grey = Utils.GetSolidColorBrush("#FF939598");

    }
}
