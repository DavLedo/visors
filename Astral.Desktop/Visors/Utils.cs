﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Astral.Device;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Point = Windows.Foundation.Point;
using TouchPoint = Astral.Device.TouchPoint;

namespace Visors
{

    public static class Utils
    {

        public static SolidColorBrush GetSolidColorBrush(string hex)
        {

            SolidColorBrush myBrush = new SolidColorBrush(GetColor(hex));
            return myBrush;
        }

        public static Color GetColor(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            byte a = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte r = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
            return Windows.UI.Color.FromArgb(a, r, g, b);
        }

        public static bool IsInsideRect(this TouchPoint p, Rect rect)
        {
            return (p.X >= rect.Left && p.X <= rect.Right && p.Y >= rect.Top && p.Y <= rect.Bottom);
        }

        public static bool IsInsideRect(this Point p, Rect rect)
        {
            return (p.X >= rect.Left && p.X <= rect.Right && p.Y >= rect.Top && p.Y <= rect.Bottom);
        }

        public static double DistanceBetweenTwoPoints(Point p1, Point p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        public static double Magnitude(double x, double y, double z)
        {
            return Math.Sqrt(x * x + y * y + z * z);
        }

        public static double Map(double value, double min, double max, double newMin, double newMax)
        {
            if (value == min)
            {
                return newMin;
            }
            return (((newMax - newMin) / (max - min)) * (value - min)) + newMin;
        }


        public static double CubicMap(double value, double min, double max, double newMin, double newMax)
        {
            double y;

            // adjust minimum and maximum so they are from 0 to 1
            double reducedX = (value - min) / (max - min);

            // apply function
            y = 0.5 * (Math.Pow((2 * reducedX - 1), 3) + 1);

            // adjust minimum and maximum to new values
            y = (newMax - newMin) * y + newMin;

            return y;
        }

        public static double RadiansToDegrees(double radians)
        {
            return (radians * 180 / Math.PI);
        }

        public static double DegreesToRadians(double degrees)
        {
            return (degrees * Math.PI / 180);
        }

        public static double Average(params double[] values)
        {
            return (double)(values.Sum() / values.Length);

        }

        public static ModuleType EventToModule(MobileEventType type)
        {
            ModuleType? moduleType = null;

            switch (type)
            {
                case MobileEventType.None:
                    break;
                case MobileEventType.TouchDown:
                    moduleType = ModuleType.Display;
                    break;
                case MobileEventType.TouchUp:
                    moduleType = ModuleType.Display;
                    break;
                case MobileEventType.TouchMove:
                    moduleType = ModuleType.Display;
                    break;
                case MobileEventType.AccelerationChanged:
                    moduleType = ModuleType.Accelerometer;
                    break;
                case MobileEventType.AmbientLightChanged:
                    moduleType = ModuleType.AmbientLight;
                    break;
                case MobileEventType.CompassChanged:
                    moduleType = ModuleType.Compass;
                    break;
                case MobileEventType.MagnetometerChanged:
                    moduleType = ModuleType.Magnetometer;
                    break;
                case MobileEventType.MagnetometerMagnitudeChanged:
                    moduleType = ModuleType.Magnetometer;
                    break;
                case MobileEventType.MagnetometerAngleChanged:
                    moduleType = ModuleType.Magnetometer;
                    break;
                case MobileEventType.GyroscopeChanged:
                    moduleType = ModuleType.Gyroscope;
                    break;
                case MobileEventType.OrientationChanged:
                    moduleType = ModuleType.Orientation;
                    break;
                case MobileEventType.AmplitudeChanged:
                    moduleType = ModuleType.Microphone;
                    break;
                case MobileEventType.FrequencyChanged:
                    moduleType = ModuleType.Microphone;
                    break;
                default:
                    break;
            }
            return moduleType.GetValueOrDefault();
        }

        private static byte Lerp(byte a, byte b, double step)
        {
            return (byte)(a + (b - a) * step);
        }

        public static Windows.UI.Color InterpolateColors(double step, Color color1, Color color2)
        {
            return Color.FromArgb(Lerp(color1.A, color2.A, step),
                                    Lerp(color1.R, color2.R, step),
                                    Lerp(color1.G, color2.G, step),
                                    Lerp(color1.B, color2.B, step));
        }

        public static Color InterpolateColors(double step, params Color[] colors)
        {
            double offset = (double)1 / (colors.Length - 1);

            if (step == 0)
            {
                return colors[0];
            }

            if (step == 1)
            {
                return colors[colors.Length - 1];
            }
            for (int i = 0; i < colors.Length - 1; i++)
            {
                double substep = i * offset;
                double substepMax = substep + offset;
                if (step > substep && step <= substepMax)
                {
                    double newStep = Utils.Map(step, substep, substepMax, 0, 1);

                    return InterpolateColors(step, colors[i], colors[i + 1]);
                }
            }

            return Colors.Transparent;

        }

    }
    
}
