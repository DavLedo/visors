﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visors
{
    public interface ICommand
    {
        void Do();
        void Undo();
    }

    public class CommandManager
    {
        private Stack<ICommand> commandStack = new Stack<ICommand>();
        private Stack<ICommand> redoStack = new Stack<ICommand>();

        public void Undo()
        {
            if (commandStack.Count != 0)
            {
                ICommand undoCommand = commandStack.Pop();
                undoCommand.Undo();
                redoStack.Push(undoCommand);
            }
        }

        public void Redo()
        {
            if (redoStack.Count != 0)
            {
                ICommand redoCommand = redoStack.Pop();
                redoCommand.Do();
                commandStack.Push(redoCommand);
            }
        }

        public void Do(ICommand command)
        {
            command.Do();
            this.commandStack.Push(command);
            this.redoStack.Clear();
        }
    }
}
