﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Visors.Drawing;

namespace Visors
{
    public class AddObjectCommand : ICommand
    {
        public Shape Shape { get; set; }
        private List<Shape> shapes;

        public void Do()
        {
            this.shapes.Add(Shape);
        }

        public void Undo()
        {
            this.shapes.Remove(Shape);
        }

        public AddObjectCommand(List<Shape> shapes, Shape shape)
        {
            this.Shape = shape;
            this.shapes = shapes;
        }
    }

    public class UpdateObjectCommand : ICommand
    {
        public Shape Shape { get; set; }
        public PropertyInfo ChangingProperty { get; set; }
        public object LastValue { get; set; }
        public object NewValue { get; set; }

        /// <summary>
        /// An update command takes in a shape, and a property it wants to update along with a new value
        /// So that executing the command updates that property
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="property"></param>
        /// <param name="newValue"></param>
        public UpdateObjectCommand(Shape shape, PropertyInfo property, object newValue)
        {
            // get the current property value
            this.LastValue = property.GetValue(shape);
            this.NewValue = newValue;
            this.Shape = shape;
            this.ChangingProperty = property;
        }

        public void Do()
        {

        }

        public void Undo()
        {

        }
    }
}
