﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.ComponentModel;
using System.Runtime.CompilerServices;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Visors
{
        public sealed partial class UIToolbar : UserControl, INotifyPropertyChanged
        {
            private UITool selectedTool = UITool.Select;

            public UITool SelectedTool
            {
                get
                {
                    return this.selectedTool;
                }
                set
                {
                    this.selectedTool = value;
                    NotifyPropertyChanged();
                }
            }


            public UIToolbar()
            {
                this.InitializeComponent();
                this.PropertyChanged += OnPropertyChanged;

                this.SelectionCanvas.PointerReleased += (s, e) => { this.SelectedTool = UITool.Select; };
                this.RectangleCanvas.PointerReleased += (s, e) => { this.SelectedTool = UITool.Rectangle; };
                this.EllipseCanvas.PointerReleased += (s, e) => { this.SelectedTool = UITool.Ellipse; };
                this.SelectedTool = UITool.Select;
            }

            private void GreyOut()
            {
                this.ArrowBG.Fill = Constants.Grey;
                this.ArrowShade.Fill = Constants.Grey;
                this.EllipseShade.Fill = Constants.Grey;
                this.EllipseBG.Fill = Constants.Grey;
                this.RectangleShade.Fill = Constants.Grey;
                this.RectangleBG.Fill = Constants.Grey;

            }

            private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                if (e.PropertyName == "SelectedTool")
                {
                    GreyOut();
                    switch (this.selectedTool)
                    {
                        case UITool.Select:
                            this.ArrowShade.Fill = Constants.Blue;
                            this.ArrowBG.Fill = Constants.Turquoise;
                            break;
                        case UITool.Rectangle:
                            this.RectangleBG.Fill = Constants.Turquoise;
                            this.RectangleShade.Fill = Constants.Blue;
                            break;
                        case UITool.Ellipse:
                            this.EllipseShade.Fill = Constants.Blue;
                            this.EllipseBG.Fill = Constants.Turquoise;
                            break;
                        default:
                            break;
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
}
