﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visors.Drawing;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Visors
{
    public static class ExtensionMethods
    {
        public static void Render(this Shape shape, CanvasDrawingSession session)
        {
            Color shapeFill = Windows.UI.Color.FromArgb(shape.Fill.A, shape.Fill.R, shape.Fill.G, shape.Fill.B);
            if (shape is Rectangle)
            {
                session.FillRectangle(new Rect(shape.X, shape.Y, shape.Width, shape.Height), shapeFill);
            }
            else if (shape is Ellipse)
            {
                session.FillEllipse(shape.X, shape.Y, shape.Width, shape.Height, shapeFill);
            }
        }
    }
}
