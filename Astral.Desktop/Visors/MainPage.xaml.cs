﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Astral;
using Astral.Device;
using System.Diagnostics;
using Visors.Drawing;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Graphics.Canvas;
using System.Diagnostics;
using System.Reflection;
using Windows.UI.ViewManagement;
using Windows.Graphics.Display;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Visors
{
    public enum UITool
    {
        Select = 0,
        Rectangle = 1,
        Ellipse = 2
    }

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {


        #region Instance Variables

        public UITool currentTool = UITool.Ellipse;
        public bool toolActivated = false;
        public Point startPoint;

        #region Window Variables

        double width;
        double height;
        ScaleTransform scale = new ScaleTransform();

        #endregion

        public CommandManager commandManager = new CommandManager();

        public List<Shape> shapes = new List<Shape>();

        public Shape activeShape;

        private NetworkManager networkManager = NetworkManager.Instance;
        private DeviceModel device;

        public UITool CurrentTool
        {
            get
            {
                return this.currentTool;
            }
            set
            {
                this.currentTool = value;
                UpdateCurrentTool();
            }
        }

        public void UpdateCurrentTool()
        {
            this.Toolbar.SelectedTool = this.CurrentTool;
        }

        #endregion

        #region Constructor and Initialization

        public MainPage()
        {
            this.InitializeComponent();
            // Start the networking
            this.networkManager.Start();
            this.networkManager.DeviceAdded += OnDeviceConnected;

            this.Loaded += OnLoaded;

            ApplicationView.PreferredLaunchViewSize = new Size(1920, 1080);
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.FullScreen;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.DrawCanvas.Draw += OnCanvasDraw;

            this.DrawCanvas.PointerPressed += OnCanvasPointerDown;
            this.PointerMoved += OnCanvasPointerMoved;
            this.PointerReleased += OnCanvasPointerReleased;

            Window.Current.CoreWindow.KeyUp += OnKeyUp;
            this.Toolbar.PropertyChanged += (s, ev) => { this.currentTool = this.Toolbar.SelectedTool; };

            Rectangle r = new Rectangle();
            r.Height = 100;
            var x = r.GetType().GetProperties();
            PropertyInfo p = x[3];
            var y = p.GetValue(r);
            Debug.WriteLine(x);


            var bounds = ApplicationView.GetForCurrentView().VisibleBounds;
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var size = new Size(bounds.Width * scaleFactor, bounds.Height * scaleFactor);
            this.Container.RenderTransform = scale;
            scale.ScaleX = 1 / scaleFactor;
            scale.ScaleY = 1 / scaleFactor;

            bool z = Windows.UI.ViewManagement.ApplicationViewScaling.TrySetDisableLayoutScaling(true);
            Window.Current.SizeChanged += Sizechanged;
            this.width = Window.Current.Bounds.Width;
            this.height = Window.Current.Bounds.Height;


        }

        private void Sizechanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            double xDiff = e.Size.Width / this.width;
            double yDiff = e.Size.Height / this.height;
            this.scale.ScaleX *= xDiff;
            this.scale.ScaleY *= xDiff;
            this.width = e.Size.Width;
            this.height = e.Size.Height;

        }

        #endregion


        #region Hotkey Management

        private void OnKeyUp(Windows.UI.Core.CoreWindow sender, Windows.UI.Core.KeyEventArgs e)
        {


            if (e.VirtualKey == Windows.System.VirtualKey.V)
            {
                this.CurrentTool = UITool.Select;
            }
            else if (e.VirtualKey == Windows.System.VirtualKey.M)
            {
                this.CurrentTool = UITool.Rectangle;
            }
            else if (e.VirtualKey == Windows.System.VirtualKey.L)
            {
                this.CurrentTool = UITool.Ellipse;
            }
            else if (sender.GetKeyState(Windows.System.VirtualKey.Control) != Windows.UI.Core.CoreVirtualKeyStates.None
                && e.VirtualKey == Windows.System.VirtualKey.Z)
            {
                Debug.WriteLine("UNDO");
                this.commandManager.Undo();
                this.DrawCanvas.Invalidate();
            }
            else if (sender.GetKeyState(Windows.System.VirtualKey.Control) != Windows.UI.Core.CoreVirtualKeyStates.None
            && e.VirtualKey == Windows.System.VirtualKey.Y)
            {
                Debug.Write("REDO");
                this.commandManager.Redo();
                this.DrawCanvas.Invalidate();
            }

            //Debug.WriteLine(this.currentTool);
        }

        #endregion

        #region Canvas Events

        private void OnCanvasPointerMoved(object sender, PointerRoutedEventArgs e)
        {
            Point currentPoint = e.GetCurrentPoint(this.DrawCanvas).Position;

            if (toolActivated)
            {
                switch (this.currentTool)
                {
                    case UITool.Select:
                        break;
                    case UITool.Rectangle:
                        this.activeShape.Width = (int)Math.Abs(this.startPoint.X - currentPoint.X);
                        this.activeShape.Height = (int)Math.Abs(this.startPoint.Y - currentPoint.Y);
                        break;

                    case UITool.Ellipse:
                        this.activeShape.Width = (int)Math.Abs(this.startPoint.X - currentPoint.X);
                        this.activeShape.Height = (int)Math.Abs(this.startPoint.Y - currentPoint.Y);

                        break;
                    default:
                        break;
                }
                this.DrawCanvas.Invalidate();
            }
        }

        private void OnCanvasPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            this.toolActivated = false;
            if (this.activeShape != null)
            {
                AddObjectCommand add = new AddObjectCommand(this.shapes, this.activeShape);

                this.commandManager.Do(add);
                this.activeShape = null;
            }
        }

        private void OnCanvasPointerDown(object sender, PointerRoutedEventArgs e)
        {
            this.toolActivated = true;
            this.startPoint = e.GetCurrentPoint(this.DrawCanvas).Position;

            switch (this.currentTool)
            {
                case UITool.Select:
                    break;
                case UITool.Rectangle:
                    this.activeShape = new Rectangle() { X = (int)this.startPoint.X, Y = (int)this.startPoint.Y };
                    break;
                case UITool.Ellipse:
                    this.activeShape = new Ellipse() { X = (int)this.startPoint.X, Y = (int)this.startPoint.Y };
                    break;
                default:
                    break;
            }
        }

        private void OnCanvasDraw(CanvasControl sender, CanvasDrawEventArgs args)
        {
            foreach (Shape shape in this.shapes)
            {
                //   SolidColorBrush fill = new SolidColorBrush(shape.Fill);
                shape.Render(args.DrawingSession);
            }

            if (activeShape != null)
            {
                this.activeShape.Render(args.DrawingSession);

            }
        }

        #endregion

        #region Connection Initialization

        private async void OnDeviceConnected(object sender, DeviceConnectedEventArgs e)
        {
            device = new DeviceModel(e.Device, e.Session);

            // Note: all networking events are running in a separate thread
            // you have to use this dispatcher invoke to do anything with the UI
            // otherwise you'll get a weird exception you can't make sense of
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                Console.WriteLine(e.Device.Name);
            });

            InitializeDeviceEvents();
        }
        int count = 0;

        private void InitializeDeviceEvents()
        {
            if (count == 0)
            {
                device.Accelerometer.AccelerationChanged += AccelerometerUpdated;
            }
            else
            {
                device.Accelerometer.AccelerationChanged += AnotherAccelerationUpdated;
            }
            count++;
        }

        #endregion

        private void AccelerometerUpdated(object sender, AstralAccelerometerEventArgs e)
        {
            Debug.WriteLine("One");
            //Console.WriteLine(e.AccelerationData.X + " :: " + e.AccelerationData.Y + " :: " + e.AccelerationData.Z);
        }

        private void AnotherAccelerationUpdated(object sender, AstralAccelerometerEventArgs e)
        {
            Console.WriteLine("Two");
        }
    }
}
