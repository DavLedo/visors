﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestingConcepts
{
    /// <summary>
    /// Interaction logic for RuleTypeSelection.xaml
    /// </summary>
    public partial class RuleTypeSelection : UserControl
    {
        private RuleType ruleType;
        public RuleType TypeForCreation
        {
            get
            {
                return this.ruleType;
            }
        }

        public RuleTypeSelection()
        {
            InitializeComponent();
            this.Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).MouseLeftButtonUp += OnLeftButtonReleased;

            foreach(UIElement element in this.Container.Children)
            {
                element.MouseLeftButtonDown += OnMouseLeftClick;
                element.MouseEnter += OnMouseEnter;
                element.MouseLeave += OnMouseLeave;
            }

        }

        private Rectangle GetRectangleFromSender(object sender)
        {
            if (sender is Rectangle)
            {
                return sender as Rectangle;
            }
            else if (sender is TextBlock)
            {
                TextBlock textBlock = sender as TextBlock;
                return (sender == DefaultText ? DefaultRect : sender == SequentialText ? SequentialRect : ConditionalRect);
            }
            return null;
        }

        private TextBlock GetTextBlockFromSender(object sender)
        {
            if (sender is TextBlock)
            {
                return sender as TextBlock;
            }
            else if (sender is Rectangle)
            {
                Rectangle rect = sender as Rectangle;
                return (sender == ConditionalRect ? ConditionalText : sender == SequentialRect ? SequentialText : DefaultText);
            }
            return null;
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            Rectangle rect = GetRectangleFromSender(sender);
            TextBlock textBlock = GetTextBlockFromSender(sender);
            rect.Fill = AstralColors.LightGray;
            textBlock.Foreground = AstralColors.Black;
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            Rectangle rect = GetRectangleFromSender(sender);
            TextBlock textBlock = GetTextBlockFromSender(sender);
 
            rect.Fill = AstralColors.Teal;
            textBlock.Foreground = AstralColors.White;
        }

        private void OnMouseLeftClick(object sender, MouseButtonEventArgs e)
        {
            this.ruleType = (sender == this.DefaultText || sender == DefaultRect ? RuleType.Default :
                             sender == this.ConditionalRect || sender == ConditionalText ? RuleType.Conditional :
                             RuleType.State);
            Console.WriteLine(this.ruleType);
        }

        private void OnLeftButtonReleased(object sender, MouseButtonEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
    }
}
