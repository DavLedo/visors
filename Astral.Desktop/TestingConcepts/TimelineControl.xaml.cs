﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestingConcepts
{
    /// <summary>
    /// Interaction logic for TimelineControl.xaml
    /// </summary>
    public partial class TimelineControl : UserControl
    {
        private Shape selection = null;
        private Point mouseStart;
        private double rangeClickOffset;

        private double minimum = -250;
        private double maximum = 250;
        private double currentMin;
        private double currentMax;

        public double Min
        {
            get
            {
                return this.minimum;
            }
            set
            {
                this.minimum = value;
                UpdateMaxMin();
            }
        }

        public double Max
        {
            get
            {
                return this.maximum;
            }
            set
            {
                this.maximum = value;
                UpdateMaxMin();
            }
        }

        public double CurrentMax
        {
            get
            {
                return this.currentMax;
            }
            set
            {
                this.currentMax = value;
                UpdateMaxMinFromValues();
            }
        }

        public double CurrentMin
        {
            get
            {
                return this.currentMin;
            }
            set
            {
                this.currentMin = value;
                UpdateMaxMinFromValues();
            }

        }

        public event EventHandler<RangeSliderChangedEventArgs> Changed;
        public event EventHandler<RangeSliderChangedEventArgs> TimelineReleased;

        private void RaiseTimelineReleased(RangeSliderChangedEventArgs e)
        {
            TimelineReleased?.Invoke(this, e);
        }

        private void RaiseChanged(RangeSliderChangedEventArgs e)
        {
            if (this.Changed != null)
            {
                Changed(this, e);
            }
        }

        public TimelineControl()
        {
            InitializeComponent();
            this.Loaded += OnLoaded;
            this.currentMin = -100;
            this.currentMax = 100;
            
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {

            // prevent the Visual Studio designer from drawing null things
            // stop rendering on the designer view
#if DEBUG
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
#endif

            Window.GetWindow(this).MouseMove += OnWindowMouseMove;
            Window.GetWindow(this).MouseLeftButtonUp += OnBoundClickReleased;

            this.LowerBoundRect.MouseLeftButtonDown += OnBoundClicked;
            this.UpperBoundRect.MouseLeftButtonDown += OnBoundClicked;
            this.RangeRect.MouseLeftButtonDown += OnBoundClicked;

            UpdateMaxMinFromValues();

        }
        
        private void OnBoundClickReleased(object sender, MouseButtonEventArgs e)
        {
            this.selection = null;
            RaiseTimelineReleased(new RangeSliderChangedEventArgs(this.currentMin, this.currentMax));
        }

        private void OnBoundClicked(object sender, MouseButtonEventArgs e)
        {
            this.selection = sender as Shape;
            if (this.selection == this.RangeRect)
            {
                this.rangeClickOffset = e.GetPosition(this.Container).X - Canvas.GetLeft(this.RangeRect);
            }
            else
            {
                mouseStart = e.GetPosition(this.selection);
            }
        }

        private void UpdateMaxMinFromValues()
        {
            double minPosition = Utils.Map(this.currentMin, this.minimum, this.maximum, 0, this.BackgroungRect.Width);
            Canvas.SetLeft(this.LowerBoundRect, minPosition - LowerBoundRect.Width / 2.0);

            double maxPosition = Utils.Map(this.currentMax, this.minimum, this.maximum, 0, this.BackgroungRect.Width);
            Canvas.SetLeft(this.UpperBoundRect, maxPosition - UpperBoundRect.Width / 2.0);

            /* double minPosition = Utils.Map(this.currentMin, this.minimum, this.maximum, 0, this.BackgroungRect.Width);
            Canvas.SetLeft(this.LowerBoundRect, minPosition);

            double maxPosition = Utils.Map(this.currentMax, this.minimum, this.maximum, 0, this.BackgroungRect.Width);
            Canvas.SetLeft(this.UpperBoundRect, maxPosition); */

            MoveRangeRectangle();

            RaiseChanged(new RangeSliderChangedEventArgs(this.currentMin, this.currentMax));
        }

        private void UpdateMaxMin()
        {
            double minPosition = Canvas.GetLeft(this.LowerBoundRect) + LowerBoundRect.Width / 2.0 ;
            this.currentMin = Utils.Map(minPosition, 0, this.BackgroungRect.Width, this.minimum, this.maximum);

            double maxPosition = Canvas.GetLeft(this.UpperBoundRect) + UpperBoundRect.Width / 2.0;
            this.currentMax = Utils.Map(maxPosition, 0, this.BackgroungRect.Width, this.minimum, this.maximum);

            /* double minPosition = Canvas.GetLeft(this.LowerBoundRect);
            this.currentMin = Utils.Map(minPosition, 0, this.BackgroungRect.Width, this.minimum, this.maximum);

            double maxPosition = Canvas.GetLeft(this.UpperBoundRect);
            this.currentMax = Utils.Map(maxPosition, 0, this.BackgroungRect.Width, this.minimum, this.maximum); */

            RaiseChanged(new RangeSliderChangedEventArgs(this.currentMin, this.currentMax));

            double range = Math.Abs(this.Max - this.Min);
            double n = this.Width / range;
            int i = 0;

            List<Line> allLines = Container.Children.OfType<Line>().ToList();
            List<UIElement> children = this.Container.Children.OfType<UIElement>().ToList();
            foreach(UIElement element in children)
            {
                if(allLines.Contains(element))
                {
                    this.Container.Children.Remove(element);
                }
            }
            Console.WriteLine(  "RANGE " + range);
            while (i <= range)
            {
                double x = i * n;
                double height = (i % 5 == 0 ? this.RangeRect.Height : this.RangeRect.Height / 2);
                // Console.WriteLine(height);
                Line line = new Line() { X1 = x, X2 = x, Y1 = 0, Y2 = height, StrokeThickness = 2, Stroke = AstralColors.Black, Opacity = 0.7 };
                // this.Container.Children.Add(line);
                this.Container.Children.Insert(0, line);
                i++;
            }
        }

        private void MoveRangeRectangle()
        {
            double startX = Canvas.GetLeft(this.LowerBoundRect) + this.LowerBoundRect.Width/2;
            double endX = Canvas.GetLeft(this.UpperBoundRect) + this.UpperBoundRect.Width/2;
            Canvas.SetLeft(this.RangeRect, startX);
            if (endX - startX > 0)
            {
                this.RangeRect.Width = endX - startX;
            }
        }

        private void OnWindowMouseMove(object sender, MouseEventArgs e)
        {
            if (selection != null)
            {
                /* double left = e.GetPosition(this.Container).X;
                double divisions = Math.Abs(this.Max - this.Min);
                double divisionLength = this.Width / divisions;
                double quadrant = ((int)(left / divisionLength));
                double offset = left % divisionLength;
                double x = quadrant * divisionLength + (offset < divisionLength / 2 ? 0 : divisionLength);

                Canvas.SetLeft(selection, x); */



                // Canvas.SetLeft(selection, x);

                /* if (this.selection != this.RangeRect)
                {
                    MoveRangeRectangle();
                }
                else
                {
                    // Canvas.SetLeft(this.LowerBoundRect, e.GetPosition(this.Container).X - this.rangeClickOffset - this.LowerBoundRect.Width/2);
                    // Canvas.SetLeft(this.UpperBoundRect, e.GetPosition(this.Container).X + this.RangeRect.Width - this.rangeClickOffset - this.UpperBoundRect.Width/2);
                    // Canvas.SetLeft(this.RangeRect, e.GetPosition(this.Container).X - this.rangeClickOffset);
                } */

                if (selection != RangeRect)
                {
                    double left = e.GetPosition(Container).X;
                    double delta = left - mouseStart.X;

                    double range = Math.Abs(Max - Min);
                    double tickStep = Width / range;

                    int closestTick = (int)Math.Round(left / tickStep);
                    int finalTick = (int)Math.Min(Math.Max(Min, closestTick), Max);

                    Canvas.SetLeft(selection, finalTick * tickStep - selection.Width / 2.0);

                    MoveRangeRectangle();
                }
                else
                {
                    double delta = e.GetPosition(selection).X - rangeClickOffset;

                    double range = Math.Abs(Max - Min);
                    double selectedRange = Math.Abs(CurrentMax - CurrentMin);
                    double tickStep = Width / range;

                    // where would the left now be
                    double newLeft = Canvas.GetLeft(selection) + delta;

                    int closestMinTick = (int)Math.Round(newLeft / tickStep);
                    int closestRightTick = (int)(closestMinTick + selectedRange);

                    int finalTick = closestMinTick;
                    if (closestMinTick < Min)
                    {
                        finalTick = 0;
                    }
                    else if (closestRightTick >= Max)
                    {
                        finalTick = (int)(closestMinTick - (closestRightTick - Max));
                    }

                    Canvas.SetLeft(LowerBoundRect, finalTick * tickStep - LowerBoundRect.Width / 2.0);
                    Canvas.SetLeft(UpperBoundRect, (finalTick + selectedRange) * tickStep - UpperBoundRect.Width / 2.0);
                    Canvas.SetLeft(RangeRect, finalTick * tickStep);
                }

                UpdateMaxMin();
            }
        }
    }
}
