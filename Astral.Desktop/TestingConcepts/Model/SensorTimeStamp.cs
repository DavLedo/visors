﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TestingConcepts
{
    public enum ParameterChangeType
    {
        GravityAccelerationX,
        GravityAccelerationY,
        GravityAccelerationZ,
        LinearAccelerationX,
        LinearAccelerationY,
        LinearAccelerationZ,
        GyroX,
        GyroY,
        GyroZ,
        OrientationPitch,
        OrientationRoll,
        OrientationYaw,
        Compass,
        MicrophoneAmplitude,
        AmbientLight,
        MagnetometerX,
        MagnetometerY,
        MagnetometerZ,
        Touch
    }

    public class SensorRecording
    {
        private List<double> values = new List<double>();
        private int historyLow = 0;
        private int historyHigh = 0;

        public ParameterChangeType Param { get; set; }

        public List<double> Values
        {
            get
            {
                return this.values;
            }
        }

        public double Maximum
        {
            get
            {
                return values.GetRange(historyLow, historyHigh - historyLow).Max();
            }
        }

        public double Minimum
        {
            get
            {
                return values.GetRange(historyLow, historyHigh - historyLow).Min();
            }
        }

        public double AbsoluteAverage
        {
            get
            {
                return values.Average();
            }
        }

        public double SelectionAverage
        {
            get
            {
                return values.GetRange(historyLow, historyHigh - historyLow).Average();
            }
        }

        public double AbsoluteRange
        {
            get
            {
                return Math.Abs(this.values.Max() - this.values.Min());
            }
        }

        public double Range
        {
            get
            {
                return Math.Abs(Maximum - Minimum);
            }
        }

        public double HistoryIndexRange
        {
            get
            {
                return this.historyHigh - this.historyLow;
            }
        }
        
        public Dictionary<int, double> LocalMaximums
        {
            get
            {
                return FindMaximums().Where(val => val.Key >= this.historyLow && val.Key <= this.historyHigh).ToDictionary(key => key.Key, key => key.Value);
            }
        }

        public Dictionary<int, double> LocalMinimums
        {
            get
            {
                return FindMinimums().Where(val => val.Key >= this.historyLow && val.Key <= this.historyHigh).ToDictionary(key => key.Key, key => key.Value);
            }
        }

        public void SetHistoryRange(int low, int high)
        {
            this.historyLow = low;
            this.historyHigh = high;
        }

        public void Add(double value)
        {
            this.values.Add(value);
            this.historyHigh = this.values.Count;
        }

        private Dictionary<int, double> FindMaximums()
        {
            Dictionary<int, double> maximums = new Dictionary<int, double>();

            if (this.values.Count == 0)
            {
                return null;
            }
            else if(this.values.Count == 1)
            {
                maximums.Add(0, this.values.First());
            }
            else
            {
                // if the first point is decreasing, we define that to be a local maximum
                if(this.values[0] > this.values[1])
                {
                    maximums.Add(0, this.values.First());
                }

                for(int i = 1; i < this.values.Count-1; i++)
                {
                    // the point is a max if i-1 and i+1 are less than i
                    if(this.values[i] > this.values[i-1] && this.values[i] > this.values[i+1])
                    {
                        maximums.Add(i, this.values[i]);
                    }
                }

                // if the last point is increasing, we define that to be a local maximum
                if(this.values.Last() > this.values[values.Count-1])
                {
                    maximums.Add(this.values.Count, this.values.Last());
                }
            }
            return maximums;
        }

        public SensorRecording(ParameterChangeType parameter)
        {
            this.Param = parameter;
        }

        public ContinuousRule ToRule()
        {
            ContinuousRule rule = new ContinuousRule(Utils.ParameterToMobileEventType(this.Param));
            rule.SourceRect = new Rect(this.Minimum, this.Minimum, this.Maximum, this.Maximum);
            rule.Type = RuleType.Default;
            rule.ArgumentInfo = (this.Param.ToString().Contains("X") ? RuleArgument.X : 
                                 this.Param.ToString().Contains("Z") ? RuleArgument.Z :
                                 this.Param.ToString().Contains("Y") && this.Param != ParameterChangeType.OrientationYaw ? RuleArgument.Y : RuleArgument.Magnitude);

            Console.WriteLine(rule.EventType + " :: " + rule.SourceRect.Left + " :: " + rule.SourceRect.Right + " :: " + rule.ArgumentInfo);

            return rule;
        }

        private Dictionary<int, double> FindMinimums()
        {
            Dictionary<int, double> minimums = new Dictionary<int, double>();

            if (this.values.Count == 0)
            {
                return null;
            }
            else if (this.values.Count == 1)
            {
                minimums.Add(0, this.values.First());
            }
            else
            {
                // if the first point is increasing, we define that to be a local minimum
                if (this.values[0] < this.values[1])
                {
                    minimums.Add(0, this.values.First());
                }

                for (int i = 1; i < this.values.Count - 1; i++)
                {
                    // the point is a min if i-1 and i+1 are greater than i
                    if (this.values[i] < this.values[i - 1] && this.values[i] < this.values[i + 1])
                    {
                        minimums.Add(i, this.values[i]);
                    }
                }

                // if the last point is decreasing, we define that to be a local minimum
                if (this.values.Last() < this.values[values.Count - 1])
                {
                    minimums.Add(this.values.Count, this.values.Last());
                }
            }
            return minimums;
        }


    }
}
