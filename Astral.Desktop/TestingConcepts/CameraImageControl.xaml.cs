﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TestingConcepts
{
    /// <summary>
    /// Interaction logic for CameraImageControl.xaml
    /// </summary>
    public partial class CameraImageControl : UserControl
    {
        public enum CameraMode
        {
            Live,
            Playback
        }

        public CameraMode activeMode = CameraMode.Live;

        private const double FrameRate = 100;

        private bool isRecording = false;
        private bool isPlaying = false;

        private DispatcherTimer recordingTimer = new DispatcherTimer();

        private int currentFrame = 0;
        private DispatcherTimer playTimer = new DispatcherTimer();

        private List<ImageSource> movie = new List<ImageSource>();

        public event EventHandler<EventArgs> RecordingStarted;
        public event EventHandler<EventArgs> RecordingCompleted;

        public DispatcherTimer RecordingTimer
        {
            get
            {
                return this.recordingTimer;
            }
        }

        public DispatcherTimer PlayTimer
        {
            get
            {
                return this.playTimer;
            }
        }


        public CameraMode ActiveMode
        {
            get
            {
                return this.activeMode;
            }
            set
            {
                this.activeMode = value;
                switch (this.activeMode)
                {
                    case CameraMode.Live:
                        this.CameraImage.Visibility = Visibility.Visible;
                        this.PlaybackImage.Visibility = Visibility.Hidden;

                        break;
                    case CameraMode.Playback:
                        this.CameraImage.Visibility = Visibility.Hidden;
                        this.PlaybackImage.Visibility = Visibility.Visible;

                        break;
                    default:
                        break;
                }
            }
        }

        public int TotalFrames
        {
            get
            {
                return this.movie.Count;
            }
        }

        public int CurrentFrame
        {
            get
            {
                return this.currentFrame;
            }
            set
            {
                if (value > 0)
                {
                    this.currentFrame = value;
                }
                if(this.currentFrame <= TotalFrames-1)
                {
                    this.PlaybackImage.Source = movie[currentFrame];
                }
            }
        }

        private void RaiseRecordingStarted(EventArgs e)
        {
            RecordingStarted?.Invoke(this, e);
        }

        private void RaiseRecordingCompleted(EventArgs e)
        {
            RecordingCompleted?.Invoke(this, e);
        }

        public CameraImageControl()
        {
            InitializeComponent();
            this.ActiveMode = CameraMode.Live;

            this.recordingTimer.Interval = TimeSpan.FromMilliseconds(FrameRate);
            this.recordingTimer.Tick += OnRecordingTimerTick;

            this.playTimer.Interval = TimeSpan.FromMilliseconds(FrameRate);
            this.playTimer.Tick += OnPlayTimerTick;

            this.LiveButton.Click += LiveButton_Click;
            this.RecordButton.Click += RecordButton_Click;
            this.PlayPauseButton.Click += PlayPauseButton_Click;
        }
        
        private void PlayPauseButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.isPlaying)
            {
                this.playTimer.Stop();
                this.isPlaying = false;
            }
            else
            {
                this.isPlaying = true;
                this.playTimer.Start();
                this.ActiveMode = CameraMode.Playback;
            }
        }

        private void OnPlayTimerTick(object sender, EventArgs e)
        {
            if(movie.Count != 0 && currentFrame < this.movie.Count)
            {
                this.PlaybackImage.Source = movie[currentFrame];
                this.currentFrame++;
                if(this.currentFrame == this.movie.Count)
                {
                    this.playTimer.Stop();
                    this.isPlaying = false;
                }
            }
        }

        private void OnRecordingTimerTick(object sender, EventArgs e)
        {
            ImageSource frame = this.CameraImage.Source.Clone();
            frame.Freeze();
            this.movie.Add(frame);
        }

        private void RecordButton_Click(object sender, RoutedEventArgs e)
        {
            if(!isRecording)
            {
                this.isRecording = true;
                this.movie.Clear();
                this.recordingTimer.Start();
                this.ActiveMode = CameraMode.Live;
                RaiseRecordingStarted(new EventArgs());
            }
            else
            {
                this.isRecording = false;
                this.recordingTimer.Stop();
                this.ActiveMode = CameraMode.Playback;
                Console.WriteLine("MOVIE: " + this.movie.Count);
                RaiseRecordingCompleted(new EventArgs());
            }
        }

        private void LiveButton_Click(object sender, RoutedEventArgs e)
        {
            this.ActiveMode = CameraMode.Live;
        }
    }
}
