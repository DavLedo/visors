﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Astral.Device;
using Astral;

namespace TestingConcepts
{
    public class VisWindowEventArgs : EventArgs
    {
        public Rule InternalRule { get; set; }
        public VisWindowEventArgs(Rule rule)
        {
            this.InternalRule = rule;
        }
    }

    /// <summary>
    /// Interaction logic for AutomationWindow.xaml
    /// </summary>
    public partial class AutomationWindow : Window
    {
        private DeviceModel deviceModel;
        private Dictionary<ParameterChangeType, SensorRecording> sensorHistory = new Dictionary<ParameterChangeType, SensorRecording>();
        private Dictionary<ParameterChangeType, double> sensorReadings = new Dictionary<ParameterChangeType, double>();

        private List<SensorTimelineControl> sensorVisualizations = new List<SensorTimelineControl>();

        private bool hasRecordedOnce = false;
        private int frameCount = 0;
        private bool shouldUpdate = false;

        private ParameterChangeType selectedParameter;

        public DeviceModel DeviceModel
        {
            get
            {
                return this.deviceModel;
            }
            set
            {
                this.deviceModel = value;
                StartDeviceModel();
            }
        }

        public event EventHandler<VisWindowEventArgs> RuleCreated;

        private void RaiseRuleCreated(VisWindowEventArgs e)
        {
            this.RuleCreated?.Invoke(this, e);
        }


        private void StartModelForParameter(ParameterChangeType type)
        {
            this.sensorReadings.Add(type, 0);
            this.sensorHistory.Add(type, new SensorRecording(type));


            SensorTimelineControl control = new SensorTimelineControl(type);
            control.MouseRightButtonDown += OnTimelineVisualizationRightClick;
            this.SensorTimelinePanel.Children.Add(control);
            this.sensorVisualizations.Add(control);
            if(type == ParameterChangeType.AmbientLight || type == ParameterChangeType.Compass || type == ParameterChangeType.MicrophoneAmplitude)
            {
                control.Plotter.StartAtZero = true;
            }
            if(type.ToString().Contains("Magnetometer"))
            {
                control.Plotter.MaxRange = 1000;
            }
        }

        private void OnTimelineVisualizationRightClick(object sender, MouseButtonEventArgs e)
        {
            (this.ContextMenu.RenderTransform as TranslateTransform).X = e.GetPosition(this.Container).X;
            (this.ContextMenu.RenderTransform as TranslateTransform).Y = e.GetPosition(this.Container).Y;
            this.ContextMenu.Visibility = Visibility.Visible;
            this.selectedParameter = (sender as SensorTimelineControl).Parameter;

            this.ContextMenu.MouseLeftButtonDown += ContextMenu_MouseLeftButtonDown;
        }

        private void ContextMenu_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(true)
            {
                Console.WriteLine(this.selectedParameter);
                Rule rule = this.sensorHistory[this.selectedParameter].ToRule();
                RaiseRuleCreated(new VisWindowEventArgs(rule));
            }
        }

        public void StartDeviceModel()
        {
            Dispatcher.Invoke(new Action(delegate
            {
                if (this.deviceModel.Device.HasAccelerometer)
                {
                    this.deviceModel.AccelerationChanged += OnAccelerationChanged;
                    StartModelForParameter(ParameterChangeType.LinearAccelerationX);
                    StartModelForParameter(ParameterChangeType.LinearAccelerationY);
                    StartModelForParameter(ParameterChangeType.LinearAccelerationZ);
                }
                if(this.deviceModel.Device.HasAmbientLight)
                { 
                    this.deviceModel.AmbientLight.AmbientLightChanged += OnLightChanged;
                    StartModelForParameter(ParameterChangeType.AmbientLight);
                }
                if(this.deviceModel.Device.HasCompass)
                {
                    this.deviceModel.Compass.HeadingChanged += OnCompassChanged;
                    StartModelForParameter(ParameterChangeType.Compass);
                }
                if(this.deviceModel.Device.HasMicrophone)
                {
                    this.deviceModel.Microphone.MicrophoneUpdated += OnMicrophoneUpdated;
                    StartModelForParameter(ParameterChangeType.MicrophoneAmplitude);
                }
                if (this.deviceModel.Device.HasMagnetometer)
                {
                    this.deviceModel.Magnetometer.MagnetometerChanged += OnMagnetometerUpdated;
                    StartModelForParameter(ParameterChangeType.MagnetometerX);
                    StartModelForParameter(ParameterChangeType.MagnetometerY);
                    StartModelForParameter(ParameterChangeType.MagnetometerZ);
                }
                if (this.deviceModel.Device.HasOrientation)
                {
                    this.deviceModel.Orientation.OrientationChanged += OnOrientationUpdated;
                    StartModelForParameter(ParameterChangeType.OrientationYaw);
                    StartModelForParameter(ParameterChangeType.OrientationPitch);
                    StartModelForParameter(ParameterChangeType.OrientationRoll);
                }
            }));

        }

        private void OnOrientationUpdated(object sender, AstralOrientationEventArgs e)
        {
            this.sensorReadings[ParameterChangeType.OrientationYaw] = e.OrientationData.YawDegrees;
            this.sensorReadings[ParameterChangeType.OrientationPitch] = e.OrientationData.PitchDegrees;
            this.sensorReadings[ParameterChangeType.OrientationRoll] = e.OrientationData.RollDegrees;
        }

        private void OnMagnetometerUpdated(object sender, AstralMagnetometerEventArgs e)
        {
            this.sensorReadings[ParameterChangeType.MagnetometerX] = e.MagnetometerData.X;
            this.sensorReadings[ParameterChangeType.MagnetometerY] = e.MagnetometerData.Y;
            this.sensorReadings[ParameterChangeType.MagnetometerZ] = e.MagnetometerData.Z;
        }

        private void OnMicrophoneUpdated(object sender, AstralMicrophoneEventArgs e)
        {
            this.sensorReadings[ParameterChangeType.MicrophoneAmplitude] = e.MicrophoneData.Amplitude;
        }

        private void OnCompassChanged(object sender, AstralCompassEventArgs e)
        {
            this.sensorReadings[ParameterChangeType.Compass] = e.CompassData.Heading;
        }

        private void OnLightChanged(object sender, AstralAmbientLightEventArgs e)
        {
            this.sensorReadings[ParameterChangeType.AmbientLight] = e.AmbientLightData.AmbientLight;
        }
        
        private void OnAccelerationChanged(object sender, AccelerationDeviceModelEventArgs e)
        {
            this.sensorReadings[ParameterChangeType.LinearAccelerationX] = e.LinearX;
            this.sensorReadings[ParameterChangeType.LinearAccelerationY] = e.LinearY;
            this.sensorReadings[ParameterChangeType.LinearAccelerationZ] = e.LinearZ;
            //this.sensorReadings[ParameterChangeType.GravityAccelerationX] = e.GravityX;
            //this.sensorReadings[ParameterChangeType.GravityAccelerationY] = e.GravityY;
            //this.sensorReadings[ParameterChangeType.GravityAccelerationZ] = e.GravityZ;

            if(shouldUpdate)
            {
                foreach (ParameterChangeType p in this.sensorHistory.Keys)
                {
                //    this.sensorHistory[p].Add(new SensorStamp() { Param = p, Value = sensorReadings[p], Stamp = this.frameCount });
                    if (p == ParameterChangeType.LinearAccelerationX)
                    {
                        //this.linearAcc = e.LinearX;
                    //    Dispatcher.Invoke(new Action(delegate { this.AccPlot.PushPoint(sensorReadings[p]);  }));
                        
                    }
                }
                shouldUpdate = false;

            }
        }

        public AutomationWindow()
        {
            InitializeComponent();
          //  this.AccPlotter.SensorType = ModuleType.Accelerometer;
            this.TimelineRect.MouseMove += OnTimelineMouseMove;

            this.TimelineRect.MouseLeftButtonDown += TimelineRect_MouseLeftButtonDown;

            this.TimelineRect.Changed += OnTimelineRangeUpdated;
            this.TimelineRect.TimelineReleased += OnTimeLineReleased;

            this.ImageControl.RecordingStarted += OnRecordingStarted;
            this.ImageControl.RecordingTimer.Tick += OnRecordingTimerTick;
            this.ImageControl.RecordingCompleted += OnRecordingCompleted;

            this.DragGrid.MouseLeftButtonDown += (s,e) => { this.DragMove(); };
            this.ExitButton.Click += (s, e) => { this.Close(); };
            this.SensorTimelinePanel.MouseEnter += (s, e) => { this.TimelineLine.Visibility = Visibility.Visible; lastX = e.GetPosition(this.TimelineRect).X; };
            this.SensorTimelinePanel.MouseLeave += (s, e) => { this.TimelineLine.Visibility = Visibility.Hidden; lastX = e.GetPosition(this.TimelineRect).X; };
            this.SensorTimelinePanel.MouseMove += OnVisualizationPanelMouseMove;
            this.MouseLeftButtonDown += (s, e) => { this.ContextMenu.Visibility = Visibility.Hidden; };
        }



        private void OnRecordingCompleted(object sender, EventArgs e)
        {
            // Console.WriteLine("MAX: " + this.sensorHistory.First().Value.LocalMaximums.Count + " :: MIN: " + this.sensorHistory.First().Value.LocalMinimums.Count + " :: " + this.sensorHistory.First().Value.Values.Count);

            this.hasRecordedOnce = true;
            // this.TimelineRect.Max = this.ImageControl.TotalFrames;
            this.TimelineRect.Max = this.ImageControl.TotalFrames - 1;
            this.TimelineRect.Min = 0;
            this.TimelineRect.CurrentMax = this.TimelineRect.Max;
            this.TimelineRect.CurrentMin = 0;

            foreach(SensorTimelineControl control in this.sensorVisualizations)
            {
                control.Plotter.DrawDividers = true;
                control.Plotter.DrawPoints();
            }
            
        }

        private void OnRecordingTimerTick(object sender, EventArgs e)
        {
            shouldUpdate = true;
            foreach (ParameterChangeType p in this.sensorHistory.Keys)
            {
                if(this.sensorHistory[p] != null)
                {
                    this.sensorHistory[p].Add(sensorReadings[p]);
                }
                //    this.sensorHistory[p].Add(new SensorRecording() { Param = p, Value = sensorReadings[p], Stamp = this.frameCount });
                //if (p == ParameterChangeType.LinearAccelerationX)
                {
                    //this.linearAcc = e.LinearX;
                    //    Dispatcher.Invoke(new Action(delegate { this.AccPlot.PushPoint(sensorReadings[p]);  }));

                    if (this.sensorVisualizations.Where(x => x.Parameter == p).ToList().Count > 0)
                    {
                        SensorTimelineControl s = this.sensorVisualizations.Where(x => x.Parameter == p).ToList().First();
                        s.Plotter.AddPoint(this.sensorReadings[p]);
                        s.Plotter.DrawPoints();
                    }
                }
            }

            this.frameCount++;
        }

        private void OnRecordingStarted(object sender, EventArgs e)
        {
            this.frameCount = 0;
        }


        private void OnTimeLineReleased(object sender, RangeSliderChangedEventArgs e)
        {
            Console.WriteLine("RELEASED");
            foreach (SensorRecording rec in this.sensorHistory.Values)
            {
                rec.SetHistoryRange((int)Math.Round(e.Low), (int)Math.Round(e.High));
            }
            Console.WriteLine("====");

            if (this.hasRecordedOnce && this.ImageControl.ActiveMode == CameraImageControl.CameraMode.Playback)
            {
                int count = 1;
                double height = this.sensorVisualizations.First().ActualHeight;
                foreach (ParameterChangeType param in this.sensorHistory.Keys)
                {
                }
            }
        }

        private void OnTimelineRangeUpdated(object sender, RangeSliderChangedEventArgs e)
        {
            //double xPos = e.GetPosition(this.TimelineRect).X;
            //if (this.ImageControl.TotalFrames != 0)
            //{
            //    int a = (int)Utils.Map(xPos, 0, this.TimelineRect.Width, 0, this.ImageControl.TotalFrames);
            //    this.ImageControl.CurrentFrame = a;
            //}

            Debug.WriteLine(e.Low + " || " + e.High);

            if (this.hasRecordedOnce)
            {
                foreach (SensorTimelineControl visualization in this.sensorVisualizations)
                {
                    visualization.ViewRange((int)Math.Round(e.Low), (int)Math.Round(e.High));

                    // Debug.WriteLine("\t" + visualization.Plotter.AddPoint);
                }


            }
        }

        private void TimelineRect_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //this.TimelineSlider.Max = this.ImageControl.TotalFrames;
            //this.TimelineSlider.Min = 0;
            //this.TimelineSlider.CurrentMax = this.TimelineSlider.Max;
            //this.TimelineSlider.CurrentMin = 0;
        }

        private void TimelineSlider_MouseMove(object sender, MouseEventArgs e)
        {
            double xPos = e.GetPosition(this.TimelineRect).X;
            if (this.ImageControl.TotalFrames != 0)
            {
                // int a = (int)Utils.Map(xPos, 0, this.TimelineRect.Width, 0, this.ImageControl.TotalFrames);
                int a = (int)Utils.Map(xPos, 0, this.TimelineRect.Width, 0, this.ImageControl.TotalFrames - 1);
                this.ImageControl.CurrentFrame = a;
            }
        }

        private void OnKey(object sender, KeyEventArgs e)
        {
            //this.TimelineSlider.Max = this.ImageControl.TotalFrames;
            //this.TimelineSlider.Min = 0;
            //this.TimelineSlider.CurrentMax = this.TimelineSlider.Max;
            //this.TimelineSlider.CurrentMin = 0;
        }

        private void VideoScrub(double xPos, double absoluteXPos, double offset = 0)
        {
            if (this.ImageControl.TotalFrames != 0)
            {
                // int a = (int)Utils.Map(xPos, 0, this.TimelineRect.Width, 0, this.ImageControl.TotalFrames);
                int a = (int)Utils.Map(xPos, 0, this.TimelineRect.Width, 0, this.ImageControl.TotalFrames - 1);

                this.ImageControl.CurrentFrame = a;
                double val = absoluteXPos - xPos;

                ((this.TimelineLine.RenderTransform as TransformGroup).
                    Children.Where(t => t is TranslateTransform).ToList()[0] as TranslateTransform).X = absoluteXPos;

                //Console.WriteLine(((this.TimelineLine.RenderTransform as TransformGroup).
                //    Children.Where(t => t is TranslateTransform).ToList()[0] as TranslateTransform).X + " :: " + offset);
                
            }
        }

        double lastX = 0;

        private void OnVisualizationPanelMouseMove(object sender, MouseEventArgs e)
        {
            double xPos = e.GetPosition(this.TimelineRect).X;
            double absoluteXPos = e.GetPosition(this.Container).X;
            
            VideoScrub(xPos, absoluteXPos, absoluteXPos - lastX);
            lastX = absoluteXPos;
        }

        private void OnTimelineMouseMove(object sender, MouseEventArgs e)
        {
            double xPos = e.GetPosition(this.TimelineRect).X;
            double absoluteXPos = e.GetPosition(this.TimelineRect).X;
            VideoScrub(xPos, absoluteXPos);
        }
    }
}
