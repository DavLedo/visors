﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AForge.Video;
using AForge.Video.DirectShow;

using System.ComponentModel;
using System.Windows;
using System.Reflection;

namespace Astral.Video
{
    #region Resolution

    public class Resolution
    {
        private VideoCaptureDevice videoCaptureDevice;

        public List<Tuple<int, int>> ResolutionValues
        {
            get
            {
                return this.videoCaptureDevice.VideoCapabilities.Select(v => new Tuple<int, int>(v.FrameSize.Width, v.FrameSize.Height)).ToList();
            }
        }


        public Tuple<int, int> CurrentResolution
        {
            get
            {
                return new Tuple<int, int>(this.videoCaptureDevice.VideoResolution.FrameSize.Width, this.videoCaptureDevice.VideoResolution.FrameSize.Height);
            }
            set
            {
                SetWidthAndHeight(value);
            }
        }

        public int Width
        {
            get
            {
                return this.videoCaptureDevice.VideoResolution.FrameSize.Width;
            }
            set
            {
                if (this.videoCaptureDevice.VideoCapabilities.Where(capability => capability.FrameSize.Width == value).Count() > 0)
                {
                    this.videoCaptureDevice.VideoResolution = this.videoCaptureDevice.VideoCapabilities.Where(capability => capability.FrameSize.Width == value).Last();
                }

            }
        }

        public int Height
        {
            get
            {
                return this.videoCaptureDevice.VideoResolution.FrameSize.Height;
            }
            set
            {
                if (this.videoCaptureDevice.VideoCapabilities.Where(capability => capability.FrameSize.Height == value).Count() > 0)
                {
                    this.videoCaptureDevice.VideoResolution = this.videoCaptureDevice.VideoCapabilities.Where(capability => capability.FrameSize.Height == value).Last();
                }
            }
        }


        private void SetWidthAndHeight(int width, int height)
        {
            if (this.videoCaptureDevice.VideoCapabilities.Where(capability => capability.FrameSize.Width == width && capability.FrameSize.Height == height).Count() > 0)
            {
                this.videoCaptureDevice.VideoResolution = this.videoCaptureDevice.VideoCapabilities.Where(capability => capability.FrameSize.Width == width && capability.FrameSize.Height == height).Last();
            }
        }

        private void SetWidthAndHeight(Tuple<int, int> widthHeightPair)
        {
            SetWidthAndHeight(widthHeightPair.Item1, widthHeightPair.Item2);
        }

        internal Resolution(VideoCaptureDevice videoCaptureDevice)
        {
            this.videoCaptureDevice = videoCaptureDevice;
            this.videoCaptureDevice.VideoResolution = this.videoCaptureDevice.VideoCapabilities.First();
        }
    }
    #endregion

    public class Camera
    {
        private VideoCaptureDevice videoCaptureDevice;

        public Resolution Resolution { get; set; }

        internal Camera(VideoCaptureDevice videoCaptureDevice)
        {
            this.videoCaptureDevice = videoCaptureDevice;
            this.Resolution = new Resolution(this.videoCaptureDevice);
            Application.Current.Exit += new ExitEventHandler(Current_Exit);
            // AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
        }

        void Current_Exit(object sender, ExitEventArgs e)
        {
            Stop();
        }

        //Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        //{
        //    using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("CameraAbstraction.AForge.Video.dll"))
        //    {
        //        using (var stream2 = Assembly.GetExecutingAssembly().GetManifestResourceStream("CameraAbstraction.AForge.Video.dll"))
        //        {
        //            byte[] assemblyData = new byte[stream.Length + stream2.Length];
        //            stream.Read(assemblyData, 0, (int)stream.Length);
        //            stream2.Read(assemblyData, (int)stream.Length, (int)stream2.Length);
        //            return Assembly.Load(assemblyData);
        //        }
        //    }
        //}

        public void Start()
        {
            this.videoCaptureDevice.Start();
            this.videoCaptureDevice.NewFrame += new NewFrameEventHandler(OnNewFrame);
        }

        public void Stop()
        {
            this.videoCaptureDevice.SignalToStop();
        }

        void OnNewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            System.Drawing.Bitmap image = (System.Drawing.Bitmap)eventArgs.Frame.Clone();
            OnFrameUpdated(new FrameUpdatedEventArgs(image.BitmapToByteArray(), image));
        }

        #region Events and Delegates

        public delegate void FrameUpdatedEventHandler(object sender, FrameUpdatedEventArgs args);

        public event FrameUpdatedEventHandler FrameUpdated;

        public virtual void OnFrameUpdated(FrameUpdatedEventArgs args)
        {
            if (FrameUpdated != null)
            {
                FrameUpdated(this, args);
            }
        }

        #endregion
    }
}
