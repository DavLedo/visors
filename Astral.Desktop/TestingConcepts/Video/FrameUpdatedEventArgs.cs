﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Astral.Video
{
    public class FrameUpdatedEventArgs : EventArgs
    {
        private byte[] _frame;
        private System.Drawing.Bitmap _bitmap;

        public byte[] CurrentFrame
        {
            get
            {
                return this._frame;
            }
        }

        public System.Drawing.Bitmap Bitmap
        {
            get
            {
                return this._bitmap;
            }
        }


        public FrameUpdatedEventArgs(byte[] currentFrame, System.Drawing.Bitmap bitmap)
        {
            this._frame = currentFrame;
            this._bitmap = bitmap;
        }
    }
}
