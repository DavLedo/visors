﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;

namespace Astral.Video
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CameraAbstraction"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CameraAbstraction;assembly=CameraAbstraction"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:CameraImage/>
    ///
    /// </summary>
    public class CameraImage : Image
    {
        private static CameraManager manager = CameraManager.Instance;
        private Camera camera;

        public static readonly DependencyProperty CurrentCameraProperty = DependencyProperty.Register("CurrentCamera", typeof(int), typeof(CameraImage), new UIPropertyMetadata(0));
        public static readonly DependencyProperty BasicResolutionProperty = DependencyProperty.Register("BasicResolution", typeof(BasicResolution), typeof(CameraImage), new UIPropertyMetadata(BasicResolution.Highest));

        //private VideoFileWriter writer = null;
        private bool isRecording = false;
        private DateTime? firstFrameTime = null;

        public int CurrentCamera
        {
            get
            {
                return (int)this.GetValue(CurrentCameraProperty);
            }
            set
            {
                if (manager.Cameras.Count > 0 && value >= 0 && value < manager.Cameras.Count)
                {
                    this.SetValue(CurrentCameraProperty, value);
                    if (camera != null)
                    {
                        camera.FrameUpdated -= new Camera.FrameUpdatedEventHandler(OnFrameUpdated);
                        camera.Stop();
                    }
                    camera = manager.Cameras[value];
                    //camera.FrameUpdated += new Camera.FrameUpdatedEventHandler(OnFrameUpdated);
                    //camera.Start();
                }
            }
        }

        public BasicResolution BasicResolution
        {
            get
            {
                return (BasicResolution)this.GetValue(BasicResolutionProperty);
            }
            set
            {
                this.SetValue(BasicResolutionProperty, value);

                if (camera != null)
                {
                    switch (value)
                    {
                        case BasicResolution.Lowest:
                            camera.Resolution.CurrentResolution = camera.Resolution.ResolutionValues.First();

                            break;

                        case BasicResolution.Medium:
                            camera.Resolution.CurrentResolution = camera.Resolution.ResolutionValues[(int)camera.Resolution.ResolutionValues.Count / 2];
                            break;

                        case BasicResolution.Highest:
                            camera.Resolution.CurrentResolution = camera.Resolution.ResolutionValues.Last();
                            break;
                    }
                }
            }
        }

        private void Initialize()
        {
            if (DesignerProperties.GetIsInDesignMode(this) == true)
            {
                Application.Current.Exit += new ExitEventHandler(Current_Exit);
                if (manager.Cameras.Count > 0)
                {
                    camera = manager.Cameras[CurrentCamera];
                    camera.Stop();
                    camera.FrameUpdated += new Camera.FrameUpdatedEventHandler(OnDesignerFrameUpdated);
                    camera.Start();
                }
            }
            else
            {
                Application.Current.Exit += new ExitEventHandler(Current_Exit);
                if (manager.Cameras.Count > 0)
                {
                    camera = manager.Cameras[CurrentCamera];
                    camera.Stop();
                    camera.FrameUpdated += new Camera.FrameUpdatedEventHandler(OnFrameUpdated);
                    camera.Start();
                }

            }
        }

        void OnDesignerFrameUpdated(object sender, FrameUpdatedEventArgs args)
        {
            BitmapImage b = args.CurrentFrame.ByteArrayToImage();
            Dispatcher.Invoke(
                new Action(delegate
                {
                    this.Source = b;
                    this.Source.Freeze();
                }));
            camera.FrameUpdated -= new Camera.FrameUpdatedEventHandler(OnDesignerFrameUpdated);
            camera.Stop();
        }

        private void OnFrameUpdated(object sender, FrameUpdatedEventArgs args)
        {
            BitmapImage b = args.CurrentFrame.ByteArrayToImage();
            Dispatcher.Invoke(
                new Action(delegate
                {
                    this.Source = b;
                    this.Source.Freeze();
                }));

            if (this.isRecording)
            {
                if (this.firstFrameTime != null)
                {
                    //     this.writer.WriteVideoFrame(args.Bitmap, DateTime.Now - this.firstFrameTime.Value);
                }
                else
                {
                    //      this.writer.WriteVideoFrame(args.Bitmap);
                    this.firstFrameTime = DateTime.Now;
                }
            }
        }

        static CameraImage()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CameraImage), new FrameworkPropertyMetadata(typeof(CameraImage)));

        }

        public CameraImage()
        {
            Initialize();
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            if (camera != null)
            {
                camera.Stop();
            }
        }
    }

    public enum BasicResolution
    {
        Lowest,
        Medium,
        Highest
    }
}
