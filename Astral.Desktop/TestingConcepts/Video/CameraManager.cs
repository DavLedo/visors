﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AForge.Video;
using AForge.Video.DirectShow;

namespace Astral.Video
{
    public class CameraManager
    {
        private static CameraManager instance;

        public static CameraManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CameraManager();
                }
                return instance;
            }
        }

        List<Camera> camera = new List<Camera>();

        public List<Camera> Cameras
        {
            get
            {
                return this.camera;
            }
        }

        private CameraManager()
        {
            this.camera.Clear();

            FilterInfoCollection videoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            foreach (FilterInfo device in videoCaptureDevices)
            {
                try
                {
                    Camera camera = new Camera(new VideoCaptureDevice(device.MonikerString));
                    this.camera.Add(camera);
                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
