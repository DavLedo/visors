﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Astral;
using Astral.Device;

namespace TestingConcepts
{
    /// <summary>
    /// Interaction logic for SensorTimelineControl.xaml
    /// </summary>
    public partial class SensorTimelineControl : UserControl
    {
        private ModuleType sensorType;

        public ParameterChangeType Parameter;

        private int low = -1;
        private int high = -1;

        public ModuleType SensorType
        {
            get
            {
                return this.sensorType;
            }
            set
            {
                this.sensorType = value;
                this.SensorButton.SensorType = value;
            }
        }

        public SensorTimelineControl()
        {
            InitializeComponent();
        }

        public SensorTimelineControl(ModuleType sensorType)
        {
            InitializeComponent();
            this.SensorType = SensorType;
        }

        public SensorTimelineControl(ParameterChangeType parameter)
        {
            InitializeComponent();
            this.SensorType = Utils.ParameterToModule(parameter);
            this.Parameter = parameter;
            this.SensorText.Text = parameter.ToString();


            if (parameter.ToString().Contains('X'))
            {
                this.Plotter.Dimension = LinearPlotterDimension.X;
                this.BackgroundRect.Fill = AstralColors.Red;
                this.SensorButton.Box.Fill = AstralColors.Red;
            }
            else if (parameter.ToString().Contains('Y'))
            {
                this.Plotter.Dimension = LinearPlotterDimension.Y;
                this.BackgroundRect.Fill = AstralColors.Orange;
                this.SensorButton.Box.Fill = AstralColors.Orange;
            }
            else if (parameter.ToString().Contains('Z'))
            {
                this.Plotter.Dimension = LinearPlotterDimension.Z;
                this.BackgroundRect.Fill = AstralColors.Teal;
                this.SensorButton.Box.Fill = AstralColors.Teal;
            }
        }

        public void ViewRange(int low, int high)
        {
            this.low = low;
            this.high = high;
            this.Plotter.BoundRange(low, high);
        }
    }
}
