﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Drawing;

namespace Visors.Drawing
{

    public class Shape : INotifyPropertyChanged
    {
        protected int x;
        protected int y;
        protected int width;
        protected int height;

        protected Color fill = Color.Red;
        protected Color stroke;

        protected double strokeWidth;        

        public int X { get { return this.x; } set { this.x = value; NotifyPropertyChanged(); } }
        public int Y { get { return this.y; } set { this.y = value; NotifyPropertyChanged(); } }
        public int Width { get { return this.width; } set { this.width = value; NotifyPropertyChanged(); } }
        public int Height { get { return this.height; } set { this.height = value; NotifyPropertyChanged(); } }
        public Color Fill { get { return this.fill; } set { this.fill = value; NotifyPropertyChanged(); } }
        public Color Stroke { get { return this.stroke; } set { this.stroke = value; NotifyPropertyChanged(); } }
        public double StrokeWidth { get { return this.strokeWidth; } set { this.strokeWidth = value; } }
        
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
